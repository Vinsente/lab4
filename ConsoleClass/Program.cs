﻿using System;
using System.Linq;

namespace ConsoleCalss;
public class Student
{
    public string Name { get; set; }
    public double Group { get; set; }
    public string Exam { get; set; }
    public double Mark { get; set; }
}
public class Journal
{
    public List<Student> people { get; set; }

    public Journal()
    {
        people = new List<Student>();

        people.Add(new Student { Name = "Кудрявцева София Сергеевна", Group = 1, Exam = "Алегбра", Mark = 5 });
        people.Add(new Student { Name = "Жижкина Марианна Васильевна", Group = 3, Exam = "Алегбра", Mark = 3 });
        people.Add(new Student { Name = "Билетова Анастасия Артемовна", Group = 2, Exam = "Алегбра", Mark = 2 });
        people.Add(new Student { Name = "Курочка Михаил Валентинович", Group = 1, Exam = "Алегбра", Mark = 3 });

        people.Add(new Student { Name = "Кудрявцева София Сергеевна", Group = 1, Exam = "Информатика", Mark = 4 });
        people.Add(new Student { Name = "Жижкина Марианна Васильевна", Group = 3, Exam = "Информатика", Mark = 4 });
        people.Add(new Student { Name = "Билетова Анастасия Артемовна", Group = 2, Exam = "Информатика", Mark = 5 });
        people.Add(new Student { Name = "Курочка Михаил Валентинович", Group = 1, Exam = "Информатика", Mark = 4 });

        people.Add(new Student { Name = "Кудрявцева София Сергеевна", Group = 1, Exam = "Физика", Mark = 3 });
        people.Add(new Student { Name = "Жижкина Марианна Васильевна", Group = 3, Exam = "Физика", Mark = 5 });
        people.Add(new Student { Name = "Билетова Анастасия Артемовна", Group = 2, Exam = "Физика", Mark = 5 });
        people.Add(new Student { Name = "Курочка Михаил Валентинович", Group = 1, Exam = "Физика", Mark = 3 });

    }
}
public class Program
{
    static Journal journal = new Journal();
    static void Main(string[] args)
    {
    }
    public static string[] MaxAverageScoreStudents()
    {
        var GroupFIOStudent = from student in journal.people
                              group student by student.Name into g
                              select new
                              {
                                  Name = g.Key,
                                  TotalScore = from student in g
                                               let total = student.Mark
                                               select total
                              };

        double MaxAverageScore = GroupFIOStudent.Max(n => n.TotalScore.Average());

        var StudentsMaxScoreAverage = from student in GroupFIOStudent
                                      where student.TotalScore.Average() == MaxAverageScore
                                      select student.Name;

        return StudentsMaxScoreAverage.ToArray();
    }
    public static double AverageScoreExams(string exam)
    {
        var AverageScoreExamQuery = from student in journal.people
                                    where student.Exam == exam
                                    let totalScore = student.Mark
                                    select totalScore;

        return AverageScoreExamQuery.Average();
    }
    public static double[] AverageScoreExamBetweenGroups(string exam)
    {
        var GroupStudents = from student in journal.people
                            group student by student.@Group into g
                            select new
                            {
                                namegroup = g.Key,
                                TotalScore = from student in g
                                             where student.Exam == exam
                                             let total = student.Mark
                                             select total

                            };

        double MaxAverageScore = GroupStudents.Max(n => n.TotalScore.Average());

        var MaxAverageScoreGroup = from student in GroupStudents
                                   where student.TotalScore.Average() == MaxAverageScore
                                   select student.namegroup;

        return MaxAverageScoreGroup.ToArray();
    }
}