using Xunit;

namespace ConsoleCalss.Test;

public class UnitTest
{
    [Fact]
    public void MaxAverageScoreStudents_Test()
    {
        string[] answer = new string[] { "���������� ����� ���������" };
        Assert.Equal(answer.ToString(), Program.MaxAverageScoreStudents().ToString());
    }
    [Fact]
    public void AverageScoreExams_Test()
    {
        double answer = 3.25;
        Assert.Equal(answer, Program.AverageScoreExams("�������"));
    }
    [Fact]
    public void AverageScoreExamBetweenGroups_Test()
    {
        double[] answer = new double[] { 3, 2 };
        Assert.Equal(answer.ToString(), Program.AverageScoreExamBetweenGroups("������").ToString());
    }
}